import React, {useState} from 'react'
import './home-page.style.scss'
import Navbar from '../../components/NavBar/Navbar'
import Header from '../../sections/Header/Header'
import Value from '../../sections/value/Value'
import Feature from '../../sections/feature/Feature'
import CallToAction from '../../sections/callToAction/CallToAction'
import TeamSection from '../../sections/Team/TeamSection'
import FunFactSection from '../../sections/funFactSection/FunFactSection'
import FooterSection from '../../sections/footer/FooterSection'


const HomePage = () => {
    return (
        <div>
            <Navbar/>
            <Header/>
            <Value/>
            <Feature/>
            <CallToAction/>
            <TeamSection/>
            <FunFactSection/>
            <FooterSection/>
        </div>
    )
}

export default HomePage
