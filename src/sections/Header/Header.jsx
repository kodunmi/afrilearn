import React from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import { Button } from 'reactstrap';
// requires a loader
import { Carousel } from 'react-responsive-carousel';
import slider1 from '../../asset/images/slider1-bg.png';
import slider2 from '../../asset/images/slider2-bg.png';
import slider3 from '../../asset/images/slider3-bg.png';

import './header.style.scss';


const Header = () => {
    return (
        <Carousel autoPlay showArrows={false} dynamicHeight={true} showStatus={false} showThumbs={false} interval={5000} infiniteLoop={true}>
            <div style={{'position':'relative'}}>
                <img className='slider-img' src={slider1} />
                <div className="slide1-text-container">
                    <h3 className='slide-text' style={style.purple}>Easily the Netflix of Education</h3>    
                    <h3 className='slide-text'>for <span style={style.lightBlue}>African students</span> </h3> 
                    <p style={{'marginTop':'10px'}}>Lorem ipsum dolor sit, amet consectetur adipisicing elit.<br/> Nulla exercitationem veritatis magnam nesciunt similique</p>
                    <Button outline={true} color='primary'>Read More</Button>
                </div>                            
            </div>
            <div>
                <img className='slider-img' src={slider2} />
                <div className="slide2-text-container">
                    <h3 className='slide-text' style={style.purple}>Easily the Netflix of Education</h3>    
                    <span className='slide-text'>for <span style={style.base}>African students</span> </span> 
                    <p style={{'marginTop':'10px'}}>Lorem ipsum dolor sit, amet consectetur adipisicing elit.<br/> Nulla exercitationem veritatis magnam nesciunt similique</p>
                    <Button outline={true} color='primary'>Read More</Button>
                </div>  
            </div>
            <div>
                <img className='slider-img' src={slider3} />
                <div className="slide2-text-container">
                    <h3 className='slide-text' style={style.purple}>Easily the Netflix of Education</h3>    
                    <span className='slide-text'>for <span style={style.base}>African students</span> </span> 
                    <p style={{'marginTop':'10px'}}>Lorem ipsum dolor sit, amet consectetur adipisicing elit.<br/> Nulla exercitationem veritatis magnam nesciunt similique</p>
                    <Button outline={true} color='primary'>Read More</Button>
                </div>
            </div>
        </Carousel>
    )
}

const style = {
    purple:{
        'color': 'rgb(111, 26, 183)'
    },

    lightBlue:{
        'color': '#2ED2E8'
    },

    base: {
        'color': '#2bb58e'
    }

}
export default Header
