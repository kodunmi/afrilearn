import React from 'react'
import './team.style.scss'
import TeamCard from '../../components/TeamCard/TeamCard'

import Team1 from '../../asset/images/team1.jpg'
import Team2 from '../../asset/images/team2.jpg'
import Team3 from '../../asset/images/team3.jpg'
import Team4 from '../../asset/images/team4.jpg'

const TeamSection = () => {
    return (
        <section class="team section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="text-center">
							<h2>Our Awesome <span>Teachers</span></h2>
							<p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri </p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-6 col-12">
						<TeamCard image={Team1} name='Rohan Jonson' description='cumque nihil impedit quo minusid quod maxime placeat facere possimus' occupation='Associate Professor' active/>
					</div>
					<div class="col-lg-3 col-md-6 col-12">
                        <TeamCard image={Team2} name='kodunmi lekan' description='cumque nihil impedit quo minusid quod maxime placeat facere possimus' occupation='Web Programmer'/>
					</div>
					<div class="col-lg-3 col-md-6 col-12">
                        <TeamCard image={Team3} name='Lusfat Roman' description='cumque nihil impedit quo minusid quod maxime placeat facere possimus' occupation='Software Engineer'/>
					</div>
					<div class="col-lg-3 col-md-6 col-12">
                        <TeamCard image={Team4} name='Nalpamb Bold' description='cumque nihil impedit quo minusid quod maxime placeat facere possimus' occupation='Mathimatician'/>
					</div>
				</div>
			</div>
		</section>
    )
}

export default TeamSection
