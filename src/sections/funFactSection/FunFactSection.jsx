import './fun-fact-section.style.scss'
import React from 'react'

const FunFactSection = () => {
    return (
        <div class="fun-facts overlay">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-6">
						<div class="single-fact">
							<i class="fa fa-institution"></i>
							<div class="number"><span class="counter">80</span>k+</div>
							<p>Active Cources</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-6">
						<div class="single-fact">
							<i class="fa fa-graduation-cap"></i>
							<div class="number"><span class="counter">33</span>k+</div>
							<p>Active Students</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-6">
						<div class="single-fact">
							<i class="fa fa-video-camera"></i>
							<div class="number"><span class="counter">278</span>+</div>
							<p>Video Cources</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-6">
						<div class="single-fact">
							<i class="fa fa-trophy"></i>
							<div class="number"><span class="counter">308</span>+</div>
							<p>Awards Won</p>
						</div>
					</div>
				</div>
			</div>
		</div>
    )
}

export default FunFactSection
