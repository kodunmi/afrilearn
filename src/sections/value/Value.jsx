import React from 'react';
import './value.style.scss';
import CoolCard from '../../components/CoolCard/CoolCard'
import { Container, Row, Col } from 'reactstrap';
import IconHolder from '../../components/IconHolder/IconHolder';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee,faBook, faBold, faBolt, faBlender } from '@fortawesome/free-solid-svg-icons'


const Value = () => {
    return (
        <div className='section-two'>
            <Row>
                <Col className='text-center' md='12' sm='12'>
                    <h3 className='text-white mx-auto text-uppercase mb-5 mt-5'>Our Core Value</h3>
                </Col>
            </Row>
            <Row>
                <Col xs="12" sm="6" md='4' lg='4'>
                    <CoolCard>
                        <IconHolder className='mb-1' color='#00FFFF'>
                            <FontAwesomeIcon icon={faBook} size="6x" spin color='white'/>
                        </IconHolder>
                        <h3 className='text-gray'>Learning</h3>
                    </CoolCard>
                </Col>
                <Col xs="12" sm="6" md='4' lg='4'>
                    <CoolCard>
                        <IconHolder className='mb-1' color='#0080FF'>
                            <FontAwesomeIcon icon={faBolt} size="6x" spin color='white'/>
                        </IconHolder>
                        <h3 className='text-gray'>Technology</h3>
                    </CoolCard>
                </Col>
                <Col xs="12" sm="6" md='4' lg='4'>
                    <CoolCard>
                        <IconHolder className='mb-1' color='#8000FF'>
                            <FontAwesomeIcon icon={faBlender} size="6x" spin color='white'/>
                        </IconHolder>
                        <h3 className='text-gray'>Innovation</h3>
                    </CoolCard>
                </Col>
            </Row>
            
        </div>
    )
}

export default Value
