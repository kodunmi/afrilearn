import React from 'react'
import './footer-section.style.scss'
import logo from '../../asset/images/logo.png'

const FooterSection = () => {
    return (
        <footer class="footer footer-overlay section">
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-12">
							<div class="single-widget about">
								<div class="logo"><a href="#"><img src={logo} alt=""/></a></div>
								<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Lorem ipsum dolor sit amet, consectetur</p>
								<ul class="list">
									<li><i class="fa fa-phone"></i>Phone: 0908883737 </li>
									<li><i class="fa fa-envelope"></i>Email: <a href="mailto:info@youremail.com">Info@afrilearn.com</a></li>
									<li><i class="fa fa-map-o"></i>Address: 211 Lekki, lagos , Nigeria</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12">
							<div class="single-widget useful-links">
								<h2>Useful Links</h2>
								<ul>
									<li><a href="#"><i class="fa fa-angle-right"></i>Home</a></li>
									<li><a href="#"><i class="fa fa-angle-right"></i>About Us</a></li>
									<li><a href="#"><i class="fa fa-angle-right"></i>Courses</a></li>
									<li><a href="#"><i class="fa fa-angle-right"></i>Events</a></li>
									<li><a href="#"><i class="fa fa-angle-right"></i>Blogs</a></li>
									<li><a href="#"><i class="fa fa-angle-right"></i>Contact</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-12">
							<div class="single-widget newsletter">
								<h2>Subscribe Newsletter</h2>
								<div class="mail">
									<p>Don't miss to  subscribe to our news feed, Get the latest updates from us!</p>
									<div class="form">
                                        <input type="email" placeholder='enter your email'/>
										<button class="button" type="submit"><i class="fa fa-envelope"></i></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="bottom-head">
								<div class="row">
									<div class="col-12">
										<ul class="social">
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
										</ul>
										<div class="copyright">
											<p>© Copyright 2020 <a href="#">AfriLearn</a>. All Rights Reserved</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
    )
}

export default FooterSection
