import React from 'react'
import { Container, Row, Col } from 'reactstrap';
import FeatureCard from '../../components/FeatureCard/FeatureCard'
import './feature.style.scss'
import FeatureImg1 from '../../asset/images/feature1.jpg'
import FeatureImg2 from '../../asset/images/feature2.jpg'
import FeatureImg3 from '../../asset/images/feature3.jpg'

const Feature = () => {
    return (
        <section className="our-features section">
			<Container className='p-5'>
				<Row>
					<Col xs="12" md='12' lg='12'>
						<div className='text-center'>
							<h2>We Provide <span className='base-color'>Educational</span> Solutions</h2>
							<p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri </p>
						</div>
					</Col>
				</Row>
				<Row>
					<Col xs="12" sm="6" md='6' lg='4'>
						<FeatureCard image={FeatureImg1} title='Online Courses Facilities' description='Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim'/>
                    </Col>
					<Col xs="12" sm="6" md='6' lg='4'>
                        <FeatureCard image={FeatureImg2} title='Student Admin Panel' description='Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim'/>
                    </Col>
					<Col xs="12" sm="6" md='6' lg='4'>
                        <FeatureCard image={FeatureImg3} title='Perfect Guidelines' description='Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim'/>
                    </Col>
                </Row>
			</Container>
		</section>
    )
}

export default Feature
