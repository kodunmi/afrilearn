import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import HomePage from './pages/home/HomePage.component'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/home" component={HomePage}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
