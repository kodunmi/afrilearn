import React from 'react'
import './feature-card.scss'

const FeatureCard = ({image, title, description}) => {
    return (
        <div className="single-feature">
            <div className="feature-head">
                <img src={image} alt=""/>
            </div>
            <h2>{title}</h2>
            <p>{description}</p>	
        </div>
    )
}

export default FeatureCard
