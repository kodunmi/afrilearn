import React from 'react'
import './team-card.style.scss'
const TeamCard = ({name, occupation, description, image, socialLink, active}) => {
    return (
    <div className={`single-team ${active ? 'active' : ''}`}>
            <img src={image} alt=""/>
            <div className="team-hover">
                <h4>{name}<span>{occupation}</span></h4>
                <p>{description}</p>
            </div>
		</div>
    )
}

export default TeamCard
