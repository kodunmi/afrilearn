import React from 'react'
import './icon-holder.style.scss';

const IconHolder = ({children, color , onclick, className}) => {
    return (
        <div className={`icon-holder ${className}`} onClick={onclick} style={{'backgroundColor': color}}> 
            {children}
        </div>
    )
}

export default IconHolder
