import React, {useState} from 'react'
import './navbar.style.scss'
import NavList from '../NavList/NavList'
import logo from '../../asset/images/logo.png'
import { Squash as Hamburger } from 'hamburger-react'
import DropDown from '../../components/DropDown/DropDown'

const Navbar = () => {
    const [isOpen, setOpen] = useState(false)

    const toggleMenu = () => setOpen(!isOpen);
    console.log(isOpen);
    return (
        <nav>
            <div className="logo">
                <img src={logo} alt="image logo"/>
            </div>
            <ul className="nav-list">
                <NavList link='home' label='Home'/>
                <NavList link='about' label='About'/>
                <NavList link='courses' label='Courses'/>
                <NavList link='books' label='Books'/>
            </ul>
            <div className='menu'>
                <Hamburger toggled={isOpen} toggle={toggleMenu} />
            </div>

            
            {isOpen ? <DropDown toggleMenu={toggleMenu} /> : ''}
        </nav>
    )
}

export default Navbar
