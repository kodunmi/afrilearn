import './drop-down.style.scss'

import React from 'react'

const DropDown = ({toggleMenu}) => {
    return (
        <div className='menu-dropdown'>
            <ul>
                <li className='menu-dropdown-list'>Home</li>
                <li className='menu-dropdown-list'>About</li>
                <li className='menu-dropdown-list'>Courses</li>
                <li className='menu-dropdown-list'>Teachers</li>
            </ul>
            <span onClick={toggleMenu} className='menu-close'>X</span>
        </div>
    )
}

export default DropDown
