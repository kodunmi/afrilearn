import React from 'react'
import './nav-list-item.style.scss'
function NavList({color, link , label}) {
    return (
        <a href={link} className="nav-list-item">{label}</a>
    )
}

export default NavList