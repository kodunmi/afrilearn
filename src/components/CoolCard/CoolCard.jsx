import React from 'react'
import './cool-card.style.scss'
const CoolCard = ({children}) => {
    return (
        <div className='cool-card-container'>
            {children}
        </div>
    )
}

export default CoolCard
